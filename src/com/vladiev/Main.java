package com.vladiev;

import com.vladiev.algorithm.AlgorithmContainer;
import com.vladiev.algorithm.fitness.SimpleFitness;
import com.vladiev.algorithm.mate.SimpleMate;
import com.vladiev.algorithm.mutation.SimpleMutation;
import com.vladiev.io.IOModule;
import com.vladiev.io.InputData;
import com.vladiev.io.OutputData;
import com.vladiev.model.Model;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class Main {


    public static void main(String[] args) throws HDF5Exception {
        String inputFileName = "inputData/input3.h5";
        String outputFileName = "outputData/output3.h5";

        InputData inputData = IOModule.read(inputFileName);
        AlgorithmContainer algorithmContainer = new AlgorithmContainer(new SimpleFitness(inputData.getTargetMatrix()),
                new SimpleMate(), new SimpleMutation());

        Model model = new Model(inputData, algorithmContainer, 10, 3);
        model.run();

        OutputData outputData = model.getResult();

        System.out.println(outputData);

        IOModule.write(outputFileName, outputData);

    }
}

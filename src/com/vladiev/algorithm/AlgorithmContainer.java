package com.vladiev.algorithm;

import com.vladiev.algorithm.fitness.Fitness;
import com.vladiev.algorithm.mate.Mate;
import com.vladiev.algorithm.mutation.Mutation;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class AlgorithmContainer {
    private Fitness fitness;
    private Mate mate;
    private Mutation mutation;


    public AlgorithmContainer(Fitness fitness, Mate mate, Mutation mutation) {
        this.fitness = fitness;
        this.mate = mate;
        this.mutation = mutation;
    }

    public Fitness getFitness() {
        return fitness;
    }

    public Mate getMate() {
        return mate;
    }

    public Mutation getMutation() {
        return mutation;
    }
}

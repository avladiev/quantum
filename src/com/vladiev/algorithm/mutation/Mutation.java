package com.vladiev.algorithm.mutation;

import com.vladiev.model.Scheme;
import com.vladiev.model.matrix.Matrix;

import java.util.List;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public interface Mutation {

    void mutate(Scheme scheme, List<Matrix> baseMatrix);

}

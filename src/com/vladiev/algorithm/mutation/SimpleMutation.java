package com.vladiev.algorithm.mutation;

import com.vladiev.model.Level;
import com.vladiev.model.Scheme;
import com.vladiev.model.matrix.Matrix;
import com.vladiev.util.Util;

import java.util.List;
import java.util.Random;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class SimpleMutation implements Mutation {
    private static final Random RANDOM = new Random();


    @Override
    public void mutate(Scheme scheme, List<Matrix> baseMatrix) {
        if (RANDOM.nextBoolean()) {
            Level level = Util.makeRandomLevel(scheme.getNumberQubit(), baseMatrix);
            List<Level> levels = scheme.getLevels();
            levels.set(levels.size() - 1, level);
        }

    }

}
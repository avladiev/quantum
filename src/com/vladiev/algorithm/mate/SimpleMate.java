package com.vladiev.algorithm.mate;

import com.vladiev.model.Level;
import com.vladiev.model.Scheme;

import java.util.ArrayList;
import java.util.List;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class SimpleMate implements Mate {

    @Override
    public void mate(List<Scheme> in, List<Scheme> out) {
        for (int j = 0; j < 3; ++j) {
            Scheme first = in.get(j);
            for (int i = j; i < in.size(); ++i) {
                out.add(mate(first, in.get(i)));
            }
        }
    }


    private Scheme mate(Scheme parent1, Scheme parent2) {
        List<Level> parent1Levels = parent1.getLevels();
        List<Level> parent2Levels = parent2.getLevels();

        List<Level> childLevels = new ArrayList<Level>(parent1Levels);
        childLevels.add(parent2Levels.get(parent2Levels.size() - 1));
        return new Scheme(parent1.getNumberQubit(), childLevels);
    }
}

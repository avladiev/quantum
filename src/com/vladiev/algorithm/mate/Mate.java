package com.vladiev.algorithm.mate;

import com.vladiev.model.Scheme;

import java.util.List;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public interface Mate {

    void  mate(List<Scheme> in, List<Scheme> out);
}

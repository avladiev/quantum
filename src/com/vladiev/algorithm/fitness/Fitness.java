package com.vladiev.algorithm.fitness;

import com.vladiev.model.matrix.Matrix;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public  interface Fitness {
    double countFitness(Matrix matrix);

}

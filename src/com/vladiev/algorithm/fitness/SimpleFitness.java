package com.vladiev.algorithm.fitness;

import com.vladiev.algorithm.fitness.Fitness;
import com.vladiev.model.matrix.Matrix;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class SimpleFitness implements Fitness {

    private Matrix targetMatrix;

    public SimpleFitness(Matrix targetMatrix) {
        this.targetMatrix = targetMatrix;
    }

    @Override
    public double countFitness(Matrix matrix) {
        Matrix sub = matrix.subtraction(targetMatrix);
        double squareNorm = countSquareNorm(sub);
        return 1 / squareNorm;
    }


    /**
     * Euclidean norm:
     *
     * @param matrix
     * @return
     */
    static double countSquareNorm(Matrix matrix) {
        double result = 0;
        int n = matrix.getNumberColumns();

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                double v = matrix.get(i, j);
                result += v * v;
            }
        }
        return result;
    }


}

package com.vladiev.util;

import com.vladiev.model.Level;
import com.vladiev.model.matrix.Matrix;

import java.util.List;
import java.util.Random;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class Util {

    private static final Random RANDOM = new Random();

    public static Level makeRandomLevel(int numberQubit, List<Matrix> baseMatrix) {
       int[] qubit2Matrix = new int[numberQubit];
       int curQubit = 0;
       int maxEmptyIteration = 5;
       int numEmptyIteration = 0;
       while (curQubit < numberQubit) {
           numEmptyIteration++;
           int matrixNumber = RANDOM.nextInt(baseMatrix.size());
           Matrix matrix = baseMatrix.get(matrixNumber);
           int numQubitInMatrix = matrix.getNumberRows() / 2;

           if (curQubit + numQubitInMatrix <= numberQubit) {
               numEmptyIteration = 0;
               for (int j = 0; j < numQubitInMatrix; ++j) {
                   qubit2Matrix[curQubit++] = matrixNumber;
               }
           } else if (numEmptyIteration > maxEmptyIteration) {
               qubit2Matrix[curQubit++] = 0;
           }
       }

       return new Level(qubit2Matrix, baseMatrix);
   }

}

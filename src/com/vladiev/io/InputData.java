package com.vladiev.io;


import com.vladiev.model.matrix.Matrix;

import java.util.Collections;
import java.util.List;

/**
 * t
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class InputData {
    private Matrix targetMatrix;
    private List<Matrix> baseMatrix;


    public InputData( Matrix targetMatrix, List<Matrix> baseMatrix) {
        this.targetMatrix = targetMatrix;
        this.baseMatrix = Collections.unmodifiableList(baseMatrix);
    }


    public int getNumberQubit() {
        return targetMatrix.getNumberColumns() / 2;
    }

    public Matrix getTargetMatrix() {
        return targetMatrix;
    }

    public List<Matrix> getBaseMatrix() {
        return baseMatrix;
    }


    @Override
    public String toString() {
        return "InputData{" +
                "targetMatrix=" + targetMatrix +
                ", baseMatrix=" + baseMatrix +
                '}';
    }
}

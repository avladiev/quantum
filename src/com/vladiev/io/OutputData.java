package com.vladiev.io;

import com.vladiev.model.matrix.Matrix;

import java.util.ArrayList;
import java.util.List;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class OutputData {
    List<int[]> qubit2MatrixInLevels = new ArrayList<int[]>();
    double fitness;
    Matrix matrix;

    public OutputData(List<int[]> levels, double fitness, Matrix matrix) {
        this.qubit2MatrixInLevels = levels;
        this.fitness = fitness;
        this.matrix = matrix;
    }

    public List<int[]> getQubit2MatrixInLevels() {
        return qubit2MatrixInLevels;
    }

    public double getFitness() {
        return fitness;
    }

    public Matrix getMatrix() {
        return matrix;
    }

    @Override
    public String toString() {
        return "OutputData{" +
                "qubit2MatrixInLevels=" + qubit2MatrixInLevels +
                ", fitness=" + fitness +
                ", matrix=" + matrix +
                '}';
    }
}

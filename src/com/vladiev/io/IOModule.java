package com.vladiev.io;


import com.vladiev.model.matrix.Matrix;
import com.vladiev.model.matrix.SimpleMatrix;
import ncsa.hdf.hdf5lib.H5;
import ncsa.hdf.hdf5lib.HDF5Constants;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;

import java.util.ArrayList;
import java.util.List;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class IOModule {

    public static InputData read(String fileName) throws HDF5Exception {
        int inputFileId = H5.H5Fopen(fileName, HDF5Constants.H5F_ACC_RDWR, HDF5Constants.H5P_DEFAULT);

        int dataSetId = H5.H5Dopen(inputFileId, "targetMatrix", HDF5Constants.H5P_DEFAULT);
        int dataSpaceId = H5.H5Dget_space(dataSetId);

        long[] dims = new long[2];

        H5.H5Sget_simple_extent_dims(dataSpaceId, dims, null);

        double[][] targetMatrix = new double[(int) dims[0]][(int) dims[1]];
        H5.H5Dread(dataSetId, HDF5Constants.H5T_NATIVE_DOUBLE,
                HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
                HDF5Constants.H5P_DEFAULT, targetMatrix);

        H5.H5Dclose(dataSetId);
        int[][] numberBaseMatrixRaw = new int[1][1];
        dataSetId = H5.H5Dopen(inputFileId, "numberBaseMatrix", HDF5Constants.H5P_DEFAULT);
        H5.H5Dread(dataSetId, HDF5Constants.H5T_NATIVE_INT,
                HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
                HDF5Constants.H5P_DEFAULT, numberBaseMatrixRaw);
        int numberBaseMatrix = numberBaseMatrixRaw[0][0];
        H5.H5Dclose(dataSetId);
        List<Matrix> basicMatrix = new ArrayList<Matrix>();

        for (int i = 0; i < numberBaseMatrix; ++i) {
            dataSetId = H5.H5Dopen(inputFileId, "baseMatrix" + i, HDF5Constants.H5P_DEFAULT);
            dataSpaceId = H5.H5Dget_space(dataSetId);
            H5.H5Sget_simple_extent_dims(dataSpaceId, dims, null);
            double[][] mat = new double[(int) dims[0]][(int) dims[1]];
            H5.H5Dread(dataSetId, HDF5Constants.H5T_NATIVE_DOUBLE,
                    HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
                    HDF5Constants.H5P_DEFAULT, mat);
            Matrix matrix = new SimpleMatrix(mat);
            basicMatrix.add(matrix);
        }
        H5.H5Dclose(dataSetId);
        H5.H5Fclose(inputFileId);
        return new InputData(new SimpleMatrix(targetMatrix), basicMatrix);
    }

    public static void write(String fileName, OutputData outputData) throws HDF5Exception {
        int fileId = H5.H5Fcreate(fileName, HDF5Constants.H5F_ACC_TRUNC,
                HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
        Matrix matrix = outputData.getMatrix();
        long dims[] = new long[]{matrix.getNumberRows(), matrix.getNumberColumns()};
        int fileSpaceId = H5.H5Screate_simple(2, dims, null);

        int dataSetId = H5.H5Dcreate(fileId, "matrix",
                HDF5Constants.H5T_IEEE_F64LE
                , fileSpaceId,
                HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);

        double[][] rawMatrix = new double[matrix.getNumberRows()][matrix.getNumberColumns()];

        for (int i = 0; i < rawMatrix.length; ++i) {
            for (int j = 0; j < rawMatrix[i].length; ++j) {
                rawMatrix[i][j] = matrix.get(i, j);
            }
        }


        H5.H5Dwrite(dataSetId, HDF5Constants.H5T_NATIVE_DOUBLE,
                HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
                HDF5Constants.H5P_DEFAULT, rawMatrix);
        H5.H5Dclose(dataSetId);
        H5.H5Sclose(fileSpaceId);

        //save fitness
        fileSpaceId = H5.H5Screate_simple(2, new long[]{1, 1}, null);

        dataSetId = H5.H5Dcreate(fileId, "fitness",
                HDF5Constants.H5T_IEEE_F64LE
                , fileSpaceId,
                HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
        H5.H5Dwrite(dataSetId, HDF5Constants.H5T_NATIVE_DOUBLE,
                HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
                HDF5Constants.H5P_DEFAULT, new double[][]{{outputData.getFitness()}});
        H5.H5Dclose(dataSetId);
        H5.H5Sclose(fileSpaceId);

        //save number Levels
        List<int[]> qubit2MatrixInLevels = outputData.getQubit2MatrixInLevels();

        fileSpaceId = H5.H5Screate_simple(2, new long[]{1, 1}, null);
        dataSetId = H5.H5Dcreate(fileId, "numberLevels",
                HDF5Constants.H5T_STD_I32LE
                , fileSpaceId,
                HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);

        H5.H5Dwrite(dataSetId, HDF5Constants.H5T_NATIVE_INT,
                HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
                HDF5Constants.H5P_DEFAULT, new int[][]{{qubit2MatrixInLevels.size()}});
        H5.H5Dclose(dataSetId);
        H5.H5Sclose(fileSpaceId);

        //save  Levels

        for (int i = 0; i < qubit2MatrixInLevels.size(); ++i) {
            int[] qubit2Matrix = qubit2MatrixInLevels.get(i);


            fileSpaceId = H5.H5Screate_simple(2, new long[]{qubit2Matrix.length,1 }, null);
            dataSetId = H5.H5Dcreate(fileId, "level"+i,
                    HDF5Constants.H5T_STD_I32LE
                    , fileSpaceId,
                    HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);

            H5.H5Dwrite(dataSetId, HDF5Constants.H5T_NATIVE_INT,
                    HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
                    HDF5Constants.H5P_DEFAULT, new int[][]{qubit2Matrix});
            H5.H5Dclose(dataSetId);
            H5.H5Sclose(fileSpaceId);

        }


        H5.H5Fclose(fileId);
    }

}

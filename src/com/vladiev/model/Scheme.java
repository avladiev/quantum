package com.vladiev.model;

import com.vladiev.algorithm.fitness.Fitness;
import com.vladiev.model.matrix.IdenticalMatrix;
import com.vladiev.model.matrix.Matrix;

import java.util.ArrayList;
import java.util.List;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class Scheme {
    int numberQubit;
    private List<Level> levels;
    private double fitness;
    private Matrix schemeMatrix;

    public Scheme(int numberQubit, Level level) {
        this.numberQubit = numberQubit;
        levels = new ArrayList<Level>();
        levels.add(level);
    }

    public Scheme(int numberQubit, List<Level> levels) {
        this.numberQubit = numberQubit;
        this.levels = levels;
    }

    public int getNumberQubit() {
        return numberQubit;
    }

    public List<Level> getLevels() {
        return levels;
    }


    public void countFitness(Fitness fitnessAlgo) {

        schemeMatrix = new IdenticalMatrix(numberQubit * 2);

        for (int i = 0; i < levels.size(); ++i) {
            Matrix levelMatrix = levels.get(i).countMatrix();
            schemeMatrix = schemeMatrix.multiplication(levelMatrix);
        }

        fitness = fitnessAlgo.countFitness(schemeMatrix);
    }

    public double getFitness() {
        return fitness;
    }

    public Matrix getSchemeMatrix() {
        return schemeMatrix;
    }
}

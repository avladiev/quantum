package com.vladiev.model.matrix;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class MatrixUtil {


    /**
     * only nxn!!!
     *
     * @param m1
     * @param m2
     * @return new matrix
     */
    public static Matrix multiplication(Matrix m1, Matrix m2) {
        int n = m1.getNumberColumns();

        SimpleMatrix result = new SimpleMatrix(new double[n][n]);

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                double sum = result.get(i, j);
                for (int k = 0; k < n; ++k) {
                    sum += m1.get(i, k) * m2.get(k, j);
                }
                result.set(i, j, sum);
            }
        }
        return result;
    }


    public static Matrix subtraction(Matrix m1, Matrix m2) {
        int n = m1.getNumberColumns();
        SimpleMatrix result = new SimpleMatrix(new double[n][n]);
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                double sub = m1.get(i, j) - m2.get(i, j);
                result.set(i, j, sub);
            }
        }
        return result;
    }

}

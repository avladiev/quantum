package com.vladiev.model.matrix;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public interface Matrix {

    double get(int i, int j);

    int getNumberRows();

    int getNumberColumns();

    Matrix multiplication(Matrix matrix);

    Matrix subtraction(Matrix matrix);

}

package com.vladiev.model.matrix;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public abstract class AbstractMatrix implements Matrix {

    @Override
    public Matrix multiplication(Matrix matrix) {
        return MatrixUtil.multiplication(this, matrix);
    }

    @Override
    public Matrix subtraction(Matrix matrix) {
        return MatrixUtil.subtraction(this, matrix);
    }



    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < getNumberRows(); ++i) {
            for (int j = 0; j < getNumberColumns(); ++j) {
                sb.append(get(i,j)).append(" ");
            }
            sb.append("\n");
        }

        return "Matrix{" +
                "matrix: \n" + sb.toString() +
                '}';
    }
}

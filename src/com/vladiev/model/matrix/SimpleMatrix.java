package com.vladiev.model.matrix;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class SimpleMatrix extends AbstractMatrix {

    private double[][] matrix;

    public SimpleMatrix(double[][] matrix) {
        this.matrix = matrix;
    }

    @Override
    public double get(int i, int j) {
        return matrix[i][j];
    }

    public void set(int i, int j, double value) {
        matrix[i][j] = value;
    }

    @Override
    public int getNumberRows() {
        return matrix.length;
    }

    @Override
    public int getNumberColumns() {
        return matrix[0].length;
    }




}

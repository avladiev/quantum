package com.vladiev.model.matrix;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class IdenticalMatrix extends AbstractMatrix {
    private int size;


    public IdenticalMatrix(int size) {
        this.size = size;
    }

    @Override
    public double get(int i, int j) {
        return i == j ? 1.0 : 0.0;
    }

    @Override
    public int getNumberRows() {
        return size;
    }

    @Override
    public int getNumberColumns() {
        return size;
    }
}

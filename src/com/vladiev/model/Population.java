package com.vladiev.model;

import com.vladiev.algorithm.AlgorithmContainer;
import com.vladiev.algorithm.fitness.Fitness;
import com.vladiev.algorithm.mate.Mate;
import com.vladiev.algorithm.mutation.Mutation;
import com.vladiev.model.matrix.Matrix;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class Population {

    int sizePopulation;
    List<Scheme> schemes;
    List<Matrix> baseMatrix;
    int numberQubit;
    Mutation mutation;
    Fitness fitness;
    Mate mate;


    private final static Comparator<Scheme> SCHEME_COMPARATOR = new Comparator<Scheme>() {
        @Override
        public int compare(Scheme o1, Scheme o2) {
            if (o1.getFitness() > o2.getFitness()) {
                return -1;
            } else if (o1.getFitness() < o2.getFitness()) {
                return 1;
            }
            return 0;
        }
    };


    public Population(AlgorithmContainer algorithmContainer,
                      List<Scheme> schemes,
                      List<Matrix> baseMatrix,
                      int numberQubit) {
        this.schemes = schemes;
        this.baseMatrix = baseMatrix;
        this.numberQubit = numberQubit;
        this.fitness = algorithmContainer.getFitness();
        this.mutation = algorithmContainer.getMutation();
        this.mate = algorithmContainer.getMate();
        this.sizePopulation = schemes.size();
    }


    public void mutate() {
        for (Scheme scheme : schemes) {
            mutation.mutate(scheme, baseMatrix);
        }
    }


    public void mate() {
        List<Scheme> newPopulation = new ArrayList<Scheme>();
        mate.mate(schemes, newPopulation);
        schemes = newPopulation;
    }

    public void selection() {

        for (Scheme scheme : schemes) {
            scheme.countFitness(fitness);
        }
        Collections.sort(schemes, SCHEME_COMPARATOR);
        List<Scheme> newPopulation = new ArrayList<Scheme>();
        for (int i = 0; i < sizePopulation; ++i) {
            newPopulation.add(schemes.get(i));
        }
        schemes = newPopulation;
    }


    public Scheme getFirstScheme() {
        return schemes.get(0);
    }

    List<Scheme> getSchemes() {
        return schemes;
    }
}

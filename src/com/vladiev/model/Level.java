package com.vladiev.model;

import com.vladiev.model.matrix.Matrix;
import com.vladiev.model.matrix.SimpleMatrix;

import java.util.List;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class Level {

    private int[] qubit2Matrix;
    private List<Matrix> baseMatrix;

    public Level(int[] qubit2Matrix, List<Matrix> baseMatrix) {
        this.qubit2Matrix = qubit2Matrix;
        this.baseMatrix = baseMatrix;
    }



    public Matrix countMatrix() {
        int numberQubit = qubit2Matrix.length;
        SimpleMatrix result = new SimpleMatrix(new double[numberQubit * 2][numberQubit * 2]);
        int curQubit = 0;
        while (curQubit < numberQubit) {
            int matrixIndex = qubit2Matrix[curQubit];
            Matrix qubitMatrix = baseMatrix.get(matrixIndex);
            for (int i = 0; i < qubitMatrix.getNumberRows(); ++i) {
                for (int j = 0; j < qubitMatrix.getNumberColumns(); ++j) {
                    result.set(curQubit * 2 + i, curQubit * 2 + j, qubitMatrix.get(i, j));
                }
            }
            curQubit+=qubitMatrix.getNumberRows()/2;
        }

        return result;
    }

    public int[] getQubit2Matrix() {
        return qubit2Matrix;
    }
}

package com.vladiev.model;

import com.vladiev.algorithm.AlgorithmContainer;
import com.vladiev.io.InputData;
import com.vladiev.io.OutputData;
import com.vladiev.model.matrix.Matrix;
import com.vladiev.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class Model {
    private static final Random RANDOM = new Random();
    private static final int MAX_FITNESS = 10000;
    private int numberStepEvolution;
    Population population;
    List<Matrix> baseMatrix;

    public Model(InputData id, int sizePopulation) {
        this(id, null, sizePopulation, 10);
    }

    public Model(InputData id, AlgorithmContainer algorithmContainer, int sizePopulation, int numberStepEvolution) {
        this.numberStepEvolution = numberStepEvolution;
        baseMatrix = id.getBaseMatrix();
        int numberQubit = id.getNumberQubit();
        List<Scheme> schemes = new ArrayList<Scheme>();
        for (int i = 0; i < sizePopulation; ++i) {
            Level level = makeRandomLevel(numberQubit, baseMatrix);
            Scheme scheme = new Scheme(numberQubit, level);
            schemes.add(scheme);
        }

        population = new Population(algorithmContainer, schemes, baseMatrix, numberQubit);
    }

    Level makeRandomLevel(int numberQubit, List<Matrix> baseMatrix) {
        return Util.makeRandomLevel(numberQubit, baseMatrix);
    }

    public void run() {

        if (!makeSelection()) {
            for (int i = 0; i < numberStepEvolution; ++i) {

                population.mutate();
                if (makeSelection()) {
                    break;
                }

                population.mate();

                if (makeSelection()) {
                    break;
                }

            }
        }
    }

    private boolean makeSelection() {
        population.selection();
        return population.getFirstScheme().getFitness() > MAX_FITNESS;

    }

    public OutputData getResult() {
        Scheme scheme = population.getFirstScheme();
        List<Level> levels = scheme.getLevels();
        List<int[]> qubit2MatrixList = new ArrayList<int[]>();
        for (Level level : levels) {
            qubit2MatrixList.add(level.getQubit2Matrix());
        }
        return new OutputData(qubit2MatrixList, scheme.getFitness(), scheme.getSchemeMatrix());
    }


}

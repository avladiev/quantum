package com.vladiev.model.matrix;

import junit.framework.TestCase;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class MatrixUtilTest extends TestCase {


    public void testMul() {

        Matrix m1 = new SimpleMatrix(new double[][]{{1, 1}, {0, 0}});
        Matrix m2 = new SimpleMatrix(new double[][]{{1, 0}, {0, 1}});

        Matrix res = MatrixUtil.multiplication(m1, m2);

        assertEquals(res.getNumberColumns(), 2);
        assertEquals(res.getNumberRows(), 2);

        assertTrue(res.get(0, 0) < 1.1);
        assertTrue(res.get(0, 0) > 0.9);


        assertTrue(res.get(0, 1) < 1.1);
        assertTrue(res.get(0, 1) > 0.9);


        assertTrue(res.get(1, 0) < 0.1);
        assertTrue(res.get(1, 0) > -0.01);

        assertTrue(res.get(1, 1) < 0.1);
        assertTrue(res.get(1, 1) > -0.01);

    }


    public void testMul2() {

        Matrix m1 = new SimpleMatrix(new double[][]{{1.1, 1.7, 3}, {9, 0, 1.2}, {1, 32, 4}});
        Matrix m2 = new SimpleMatrix(new double[][]{{1, 8, 5}, {1, 4, 75}, {1, 2, 8}});

        Matrix res = MatrixUtil.multiplication(m1, m2);

        assertEquals(res.getNumberColumns(), 3);
        assertEquals(res.getNumberRows(), 3);

        System.out.println(res);
        /*5.8 21.6 157.0
10.2 74.4 54.6
37.0 144.0 2437.0 */

    }


    public void testSub() {
        Matrix m1 = new SimpleMatrix(new double[][]{{1.1, 1.7, 3}, {9, 0, 1.2}, {1, 32, 4}});
        Matrix m2 = new SimpleMatrix(new double[][]{{1, 8, 5}, {1, 4, 75}, {1, 2, 8}});

        Matrix res = MatrixUtil.subtraction(m1, m2);

        assertEquals(res.getNumberColumns(), 3);
        assertEquals(res.getNumberRows(), 3);

        System.out.println(res);
        /*0.10000000000000009 -6.3 -2.0
8.0 -4.0 -73.8
0.0 30.0 -4.0 */
    }
}

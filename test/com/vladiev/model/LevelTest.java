package com.vladiev.model;

import com.vladiev.io.IOModule;
import com.vladiev.io.InputData;
import com.vladiev.model.matrix.Matrix;
import junit.framework.TestCase;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;

import java.util.List;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class LevelTest extends TestCase {

    public void test1() throws HDF5Exception {
        String fileName = "data/input1.h5";
        InputData inputData = IOModule.read(fileName);
        Model model = new Model(inputData, 9);

        Population population = model.getPopulation();
        List<Scheme> schemes = population.getSchemes();
        for (Scheme scheme : schemes) {
            List<Level> levels = scheme.getLevels();
            for (Level level : levels) {
                Matrix matrix = level.countMatrix();
                System.out.println(matrix);
            }
        }
    }


    public void test2() throws HDF5Exception {
        String fileName = "data/input2.h5";
        InputData inputData = IOModule.read(fileName);
        Model model = new Model(inputData, 9);

        Population population = model.getPopulation();
        List<Scheme> schemes = population.getSchemes();
        for (Scheme scheme : schemes) {
            List<Level> levels = scheme.getLevels();
            for (Level level : levels) {
                Matrix matrix = level.countMatrix();
                System.out.println(matrix);
            }
        }
    }

    public void test3() throws HDF5Exception {
        String fileName = "data/input3.h5";
        InputData inputData = IOModule.read(fileName);
        Model model = new Model(inputData, 9);

        Population population = model.getPopulation();
        List<Scheme> schemes = population.getSchemes();
        for (Scheme scheme : schemes) {
            List<Level> levels = scheme.getLevels();
            for (Level level : levels) {
                Matrix matrix = level.countMatrix();
                System.out.println(matrix);
            }
        }
    }

}

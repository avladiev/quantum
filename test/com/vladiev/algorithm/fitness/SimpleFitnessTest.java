package com.vladiev.algorithm.fitness;

import com.vladiev.model.matrix.Matrix;
import com.vladiev.model.matrix.SimpleMatrix;
import junit.framework.TestCase;

/**
 * author Vladiev Aleksey (avladiev2@gmail.com)
 */
public class SimpleFitnessTest extends TestCase {


    public void testNorma() {
        Matrix matrix = new SimpleMatrix(new double[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        double res =  SimpleFitness.countSquareNorm(matrix);
        assertEquals((int)res,285);
    }
}
